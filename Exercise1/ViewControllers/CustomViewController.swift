//
//  CustomViewController.swift
//  Exercise1
//
//  Created by User on 20/06/15.
//  Copyright (c) 2015 L.M4rx. All rights reserved.
//

import UIKit
import Argo
import Runes


class CustomViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    
    @IBOutlet weak var tableView: UITableView!
    
    var tableList: [Entry] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.estimatedRowHeight = 68.0
        tableView.rowHeight = UITableViewAutomaticDimension
        
        let path = NSBundle.mainBundle().pathForResource("new-pods", ofType: "json")
        let data = NSData(contentsOfFile: path!)
        
        var parseError: NSError?
        let parsedObject: AnyObject? = NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.AllowFragments,
            error:&parseError)
        
        if let jsonData = parsedObject as? NSDictionary {
            if let responseData = jsonData["responseData"] as? NSDictionary {
                if let feed = responseData["feed"] as? NSDictionary {
                    if let entries = feed["entries"] as? Array<NSDictionary> {
                        for entryJson in entries {
                            let entry: Entry? = decode(entryJson)
                            tableList.append(entry!)
                        }
                    }
                }
            }
        }
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableList.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let identifier = Reusable.episodeCell.identifier
        let cell = tableView.dequeueReusableCellWithIdentifier(identifier!, forIndexPath: indexPath) as! UITableViewCell
        
        cell.detailTextLabel?.numberOfLines = 0
        
        let row = indexPath.row
        cell.textLabel?.text = tableList[row].title
        
        let contentSnippet: String = tableList[row].contentSnippet
        let contentSnippetArr = contentSnippet.componentsSeparatedByString("\n")
        cell.detailTextLabel?.text = contentSnippetArr[0]
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let row = indexPath.row
        UIApplication.sharedApplication().openURL(NSURL(string: tableList[row].link)!)
        
    }
    
    func tableView(tableView: UITableView, didDeselectRowAtIndexPath indexPath: NSIndexPath) {
        
    }
    
    func tableView(tableView: UITableView, editActionsForRowAtIndexPath indexPath: NSIndexPath) -> [AnyObject]? {
        var addReadlingList = UITableViewRowAction(style: .Normal, title: "Add to Reading List") { (action, indexPath) -> Void in
            tableView.editing = false
            println("addReadingList")
        }
        
        addReadlingList.backgroundColor = UIColor.blueColor()
        
        var star = UITableViewRowAction(style: .Normal, title: "Star") { (action, indexPath) -> Void in
            tableView.editing = false
            println("star")
        }
        
        star.backgroundColor = UIColor.orangeColor()
        
        return [star, addReadlingList]
    }
    
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
    }
}
