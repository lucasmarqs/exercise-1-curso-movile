//
//  EntryStruct.swift
//  Exercise2ndWeek
//
//  Created by User on 20/06/15.
//  Copyright (c) 2015 L.M4rx. All rights reserved.
//

import Argo
import Runes

struct Entry {
    let title: String
    let link: String
    let publishedDate: String
    let contentSnippet: String
    let content: String
}

extension Entry: Printable {
    var description: String {
        return "title: \(title), link: \(link), content: \(content)"
    }
}

extension Entry: Decodable {
    
    static func create(title: String)(link: String)(publishedDate: String)(contentSnippet: String)(content: String) -> Entry {
        return Entry(title: title, link: link, publishedDate: publishedDate, contentSnippet: contentSnippet, content: content)
    }
    
    static func decode(j: JSON) -> Decoded<Entry> {
        return create
            <^> j <| "title"
            <*> j <| "link"
            <*> j <| "publishedDate"
            <*> j <| "contentSnippet"
            <*> j <| "content"
    }
}